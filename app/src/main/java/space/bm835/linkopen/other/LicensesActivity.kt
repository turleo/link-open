package space.bm835.linkopen.other

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView
import space.bm835.linkopen.R

import kotlinx.android.synthetic.main.activity_licenses.*

class LicensesActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_licenses)

        val myBrowser = findViewById<WebView>(R.id.web)

        myBrowser.loadUrl("file:///android_asset/licenses.html")
    }

}
