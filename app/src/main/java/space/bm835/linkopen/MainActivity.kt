package space.bm835.linkopen

import android.content.Intent
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.widget.Toast


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val uri = intent.data

        val share: Button = findViewById(R.id.button3)
        val open: Button = findViewById(R.id.button)

        var url = ""
        if (uri != null) {
            url = uri.toString()
        }

        val out = findViewById<TextView>(R.id.textView2)
        out.text = url

        out.setOnClickListener {
            val clipboard = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            val clip = ClipData.newPlainText("label", url)
            clipboard.setPrimaryClip(clip)

            val toast = Toast.makeText(
                applicationContext,
                "Copied!",
                Toast.LENGTH_SHORT
            )
            toast.show()
        }

        share.setOnClickListener {
            val intent = Intent()
            intent.action = Intent.ACTION_SEND
            intent.putExtra(Intent.EXTRA_TEXT, url)
            intent.type = "text/plain"
            startActivity(intent)
        }

        open.setOnClickListener {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
            startActivity(browserIntent)
        }
    }
}
